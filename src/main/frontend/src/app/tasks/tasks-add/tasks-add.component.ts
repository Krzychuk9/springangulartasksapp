import {Component, OnInit} from '@angular/core';
import {TaskService} from "../task.service";
import {Task} from "../task.model";

@Component({
    selector: 'app-tasks-add',
    templateUrl: './tasks-add.component.html',
    styleUrls: ['./tasks-add.component.css']
})
export class TasksAddComponent implements OnInit {

    addTaskValue: String = null;

    constructor(private taskService: TaskService) {
    }

    ngOnInit() {
    }

    onTaskAdd(event) {
        const name = event.target.value;
        const task: Task = new Task(name, false, this.getTodayAsString());
        this.taskService.addTask(task).subscribe(
            (newTask: Task) => {
                this.addTaskValue = null;
                this.taskService.onTaskAdded.emit(newTask);
            }
        );
    }

    getTodayAsString(): string {
        let today = new Date();
        let dd: any = today.getDate();
        let mm: any = today.getMonth();
        let yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        return mm + '/' + dd + '/' + yyyy;
    }

}
