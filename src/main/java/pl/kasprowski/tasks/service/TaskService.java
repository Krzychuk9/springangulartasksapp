package pl.kasprowski.tasks.service;

import pl.kasprowski.tasks.domain.Task;

public interface TaskService {
    Iterable<Task> list();

    Task save(Task task);
}
