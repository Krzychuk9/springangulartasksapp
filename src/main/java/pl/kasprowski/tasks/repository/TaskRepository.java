package pl.kasprowski.tasks.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.tasks.domain.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
}
